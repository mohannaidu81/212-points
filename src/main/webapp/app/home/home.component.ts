import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Account, LoginModalService, Principal } from '../shared';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.scss'
    ]

})
export class HomeComponent implements OnInit, OnDestroy {
    account: Account;
    modalRef: NgbModalRef;
    pointsThisWeek: any = {};
  	pointsPercentage: number;
	eventSubscriber: Subscription;

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: EventManager,
        private pointsService: PointsService
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
    }
    
    ngOnDestroy() {
	  this.eventManager.destroy(this.eventSubscriber);
	}

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
        
        this.eventSubscriber = this.eventManager.subscribe('pointsListModification', ()=> this.getUserData());
	  	this.eventSubscriber = this.eventManager.subscribe('bloodPressureListModification', () => this.getUserData());
	  	this.eventSubscriber = this.eventManager.subscribe('weightListModification', ()=> this.getUserData());
	  }
    }
    
    getUserData() {
  		// Get points for the current week
 		 this.pointsService.thisWeek().subscribe((points: any) => {
	  		points = points.json;
	  		this.pointsThisWeek = points;
	 		 this.pointsPercentage = (points.points / 21) * 100;
  		});
  	}

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }
}
