import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Bloodpressure e2e test', () => {

    let navBarPage: NavBarPage;
    let bloodpressureDialogPage: BloodpressureDialogPage;
    let bloodpressureComponentsPage: BloodpressureComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Bloodpressures', () => {
        navBarPage.goToEntity('bloodpressure');
        bloodpressureComponentsPage = new BloodpressureComponentsPage();
        expect(bloodpressureComponentsPage.getTitle()).toMatch(/twentyOnePointsApp.bloodpressure.home.title/);

    });

    it('should load create Bloodpressure dialog', () => {
        bloodpressureComponentsPage.clickOnCreateButton();
        bloodpressureDialogPage = new BloodpressureDialogPage();
        expect(bloodpressureDialogPage.getModalTitle()).toMatch(/twentyOnePointsApp.bloodpressure.home.createOrEditLabel/);
        bloodpressureDialogPage.close();
    });

    it('should create and save Bloodpressures', () => {
        bloodpressureComponentsPage.clickOnCreateButton();
        bloodpressureDialogPage.setDateTimeInput(12310020012301);
        expect(bloodpressureDialogPage.getDateTimeInput()).toMatch('2001-12-31T02:30');
        bloodpressureDialogPage.setSystolicInput('5');
        expect(bloodpressureDialogPage.getSystolicInput()).toMatch('5');
        bloodpressureDialogPage.setDiastolicInput('5');
        expect(bloodpressureDialogPage.getDiastolicInput()).toMatch('5');
        bloodpressureDialogPage.userSelectLastOption();
        bloodpressureDialogPage.save();
        expect(bloodpressureDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class BloodpressureComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-bloodpressure div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class BloodpressureDialogPage {
    modalTitle = element(by.css('h4#myBloodpressureLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    dateTimeInput = element(by.css('input#field_dateTime'));
    systolicInput = element(by.css('input#field_systolic'));
    diastolicInput = element(by.css('input#field_diastolic'));
    userSelect = element(by.css('select#field_user'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDateTimeInput = function (dateTime) {
        this.dateTimeInput.sendKeys(dateTime);
    }

    getDateTimeInput = function () {
        return this.dateTimeInput.getAttribute('value');
    }

    setSystolicInput = function (systolic) {
        this.systolicInput.sendKeys(systolic);
    }

    getSystolicInput = function () {
        return this.systolicInput.getAttribute('value');
    }

    setDiastolicInput = function (diastolic) {
        this.diastolicInput.sendKeys(diastolic);
    }

    getDiastolicInput = function () {
        return this.diastolicInput.getAttribute('value');
    }

    userSelectLastOption = function () {
        this.userSelect.all(by.tagName('option')).last().click();
    }

    userSelectOption = function (option) {
        this.userSelect.sendKeys(option);
    }

    getUserSelect = function () {
        return this.userSelect;
    }

    getUserSelectedOption = function () {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
